#!/usr/bin/env python
# pylint: disable=invalid-name

from datetime import datetime
import json

from flask import Flask, jsonify, abort, request
import cx_Oracle
import requests

app = Flask(__name__)

#1. feladat: visszaadja az adatbazisban talalhato szemelyeket
@app.route('/members.json')
def list_members():
    conn = get_db()
    try:
        cur = conn.cursor()
        try:
            cur.execute('SELECT memberid, name, dateofbirth FROM members')
            queryRes = []
            for memberid, name, dateofbirth in cur:
                queryRes.append({"MemberID": memberid, "Name": name, "DateofBirth": dateofbirth.date().isoformat()})
            return jsonify(member=queryRes)
        finally:
            cur.close()
    finally:
        conn.close()

#1. feladat: visszaadja a parameterkent megadott ID-val rendelkezo szemely adatait
#2. feladat: Kiegesziti a visszaadott adatokat a lakcim koordinataival
@app.route('/members/<memberid>.json')
def member_by_id(memberid):
    conn = get_db()
    try:
        cur = conn.cursor()
        try:
            cur.execute('SELECT name, address, dateofbirth, dateofcreation, levelofmember FROM members WHERE memberid = :id', id=memberid)
            queryRes = cur.fetchone()
            if queryRes is None:
                abort(404)
            else:
                try:
                    city = queryRes[1]
                    #a vesszo utani reszeket levagjuk a cimbol
                    separator = ','
                    city = city.split(separator, 1)[0]
                    params = {
                        'action': 'query',
                        'format': 'json',
                        'limit' : 1,
                        'q': city
                        }
                    coordinates = requests.get('http://nominatim.openstreetmap.org/search', params=params)
                    for i in coordinates.json():
                        latitude = i['lat']
                        longitude = i['lon']
                except IOError:
                    pass

                data={"Name": queryRes[0], "DateofBirth": queryRes[2].date().isoformat(), "LevelofMember": queryRes[4], "DateofCreation": queryRes[3].date().isoformat(), "Address": queryRes[1], "Latitude": latitude, "Longitude": longitude}
                return jsonify(data)
        finally:
            cur.close()
    finally:
        conn.close()


#4. feladat: visszaadja a parameterrel reszben vagy egeszben egyezo szintu tagokat
@app.route('/members/by-level/<levelofmember>.json')
def members_by_level(levelofmember):
    conn = get_db()
    try:
        cur = conn.cursor()
        try:
            levelofmember = levelofmember.upper()
            levelofmember = levelofmember.replace('%', '\\\\%')
            levelofmember = "%" + levelofmember
            levelofmember = levelofmember + "%"
            cur.execute('SELECT memberid, name, dateofbirth FROM members WHERE levelofmember LIKE :l', l=levelofmember)
            queryRes = []
            for memberid, name, dateofbirth in cur:
                queryRes.append({"MemberID": memberid, "Name": name, "DateofBirth": dateofbirth.date().isoformat()})
            if queryRes is None:
                abort(404)
            else:
                return jsonify(members = queryRes)
        finally:
            cur.close()
    finally:
        conn.close()

#4. feladat: visszaadja a parameterrel reszben vagy egeszben egyezo nevo tagokat
@app.route('/members/by-name/<name>.json')
def members_by_name(name):
    conn = get_db()
    try:
        cur = conn.cursor()
        try:
            name = name.lower()
            name = name.replace('%', '\\\\%')
            name = name.replace('-', '%')
            name = "%" + name
            name = name + "%"
            cur.execute('SELECT memberid, name, dateofbirth FROM members WHERE LOWER(name) LIKE :n', n=name)
            queryRes = []
            for memberid, name, dateofbirth in cur:
                queryRes.append({"MemberID": memberid, "Name": name, "DateofBirth": dateofbirth.date().isoformat()})
            if queryRes is None:
                abort(404)
            else:
                return jsonify(members = queryRes)
        finally:
            cur.close()
    finally:
        conn.close()


@app.route('/szemelyek.json')
def list_people():
    """Lists the first 50 persons in the database"""
    conn = get_db()
    try:
        cur = conn.cursor()
        try:
            # Note: don't use prefixes like "oktatas." above for tables
            # within your own schema, as it ruins portability.
            # This table has 10k rows, so we intentionally limit the result set to 50
            # (Oracle note: not the first 50 rows by name, but rather
            # the first 50 rows of the table, which are then ordered by name).
            # Also, long queries can be broken into two shorter lines like this
            cur.execute('''SELECT szemelyi_szam, nev FROM oktatas.szemelyek
                WHERE ROWNUM < 50 ORDER BY nev ASC''')
            # there's a better way, but outside the scope of this lab:
            # http://docs.python.org/2/tutorial/datastructures.html#list-comprehensions
            results = []
            # we make use of the fact that
            #  - cursors are iterable and
            #  - `for` can unpack objects returned by each iteration
            for szemelyi_szam, nev in cur:
                results.append({'szemelyi_szam': szemelyi_szam, 'nev': nev})
            return jsonify(szemelyek=results)
        finally:
            cur.close()
    finally:
        # this is also a naive implementation, a more Pythonic solution:
        # http://docs.python.org/2/library/contextlib.html#contextlib.closing
        conn.close()


@app.route('/szemely/<szemelyi_szam>.json')
def show_person(szemelyi_szam):
    """Shows the details of a single person by szemelyi_szam"""
    conn = get_db()
    try:
        cur = conn.cursor()
        try:
            # Note: don't use prefixes like "oktatas." above for tables
            # within your own schema, as it ruins portability
            cur.execute('SELECT nev FROM oktatas.szemelyek WHERE szemelyi_szam = :sz',
                        sz=szemelyi_szam)
            # fetchone() returns a single row if there's one, otherwise None
            result = cur.fetchone()
            # in Python '==' compares by value, 'is' compares by reference
            # (of course, former would work too, but it's slower and unnecessary)
            # 'None' is the Python version of null, it's a singleton object, so
            # we can safely compare to it using 'is' (Java/C#: result == null)
            if result is None:
                # no rows -> 404 Not Found (no need to return manually)
                abort(404)
            links = []
            try:
                # we query the Wikipedia API to see what happened the day
                # the person was born based on szemelyi_szam
                born = datetime.strptime(szemelyi_szam[1:7], '%y%m%d')
                params = {
                    'action': 'query',
                    # 2012-04-01 -> "April 01" -> "April 1"
                    'titles': born.strftime('%B %d').replace('0', ''),
                    'prop': 'extlinks',
                    'format': 'json',
                    }
                # API docs: http://www.mediawiki.org/wiki/API:Tutorial
                # Example for 1st April:
                # https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extlinks&titles=April%201
                res = requests.get('https://en.wikipedia.org/w/api.php', params=params)
                for page in res.json()['query']['pages'].itervalues():
                    for link in page['extlinks']:
                        for href in link.itervalues():
                            links.append(href)
            except IOError:
                pass # necessary if a clause would be empty in Python

            # result set rows can be indexed too
            return jsonify(nev=result[0], links=links)
        finally:
            cur.close()
    finally:
        conn.close()


@app.route('/datetest.json')
def date_test():
    """Demonstrates handling dates from databases and formatting it according to ISO 8601"""
    conn = get_db()
    try:
        cur = conn.cursor()
        try:
            # Note: don't use prefixes like "oktatas." above for tables
            # within your own schema, as it ruins portability
            # http://www.oracle.com/technetwork/articles/dsl/prez-python-timesanddates-093014.html
            # https://docs.python.org/2/library/datetime.html
            # it's casted automatically to datetime
            cur.execute('SELECT datum, usd FROM oktatas.mnb_deviza where id < 10')
            results = []
            for datum, usd in cur:
                results.append({'datum': datum, 'datum_iso' : datum.isoformat(), 'usd': usd})
            return jsonify(arfolyamok=results)
        finally:
            cur.close()
    finally:
        conn.close()


@app.route('/verbtest.json', methods=['PUT', 'POST'])
def verb_test():
    """Lets you test HTTP verbs different from GET, expects and returns data in JSON format"""
    # it also shows you how to access the method used and the decoded JSON data
    return jsonify(method=request.method, data=request.get_json(), url=request.url)


def get_db():
    """Connects to the RDBMS and returns a connection object"""
    # when used with a `file` object, `with` ensures it gets closed
    # pylint: disable=no-member
    with file('config.json') as config_file:
        config = json.load(config_file)
    return cx_Oracle.connect(config['user'], config['pass'], config['host'])


if __name__ == "__main__":
    # pylint: disable=bad-option-value,wrong-import-position,wrong-import-order
    import os
    os.environ['NLS_LANG'] = '.UTF8'
    app.run(debug=True, port=os.getuid() + 10000)